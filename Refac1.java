package refactorizaciones;
/*
 * Clase para refactorizar
 * a�adida codificacion UTF-8
 * Codificacion UTF-8
 * Paquete refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea de de bloque.
 */

//eliminado java.* y a�adida la scanner para hacer el codigo mas peque�o
import java.util.Scanner;
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refac1 {
	final static String CADENAINICIO = "Bienvenido al programa";
	static Scanner in = new Scanner(System.in);
	public static void main(String[] args) {
		//Declaramos la variables estaticas y con el nombre aclarativo
				
		
		System.out.println(CADENAINICIO);
		
		
		System.out.println("Introduce tu dni");
		String dni = in.nextLine();
		
		System.out.println("Introduce tu nombre");
		String nombre = in.nextLine();
	
		//muevo las variables a donde hay que utilizarlas
		in.close();
		int numero1 = 7;
		 
		int numero2= 16;
		
		int numerototal = 25;
		//separaciones y movida la llave del if
		if(numero1 > numero2 || numerototal % 5 != 0 && (numerototal * 3 - 1) > numero2 / numerototal){
			System.out.println("Se cumple la condici�n");
		}
		
		numerototal = numero1 + numero2 * numerototal + numero2 / numero1;
		
		String [] array = new String[]{"Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo"};
		
		//llevamos a metodo el array para comprobar el dia que es
		
			mostrardias(array);
	}
	static void mostrardias(String[] vectordestrings){
		
		for(int dia = 0 ; dia < 7 ; dia++){
			System.out.println("El dia de la semana en el que te encuentras [" + (dia+1) + "-7] es el dia:\t" + vectordestrings[dia]);
		}
	}
	
}
