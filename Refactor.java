package ejr2;
/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

import java.util.Scanner;;
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refactor {
//colocar las llaves en donde le corresponden
//colocar la variables donde se usan
//poner el nombre mas claro de las variables
	static Scanner in= new Scanner(System.in);
	public static void main(String[] args){
	
		
		int []CantidadAlum = new int[10];
		//colocar las varialbbes en su sitio
		for(int n=0;n<10;n++){
			System.out.println("Introduce nota media de alumno");
			CantidadAlum[n] = in.nextInt();
		}	
		
		System.out.println("El resultado es: " + media(CantidadAlum));
		
		in.close();
	}
	//Poner llaves en su luegar y llevar al metodo la media
	 static double media(int v[]){
		double media=0;
		for(int a=0;a<10;a++) {
			media=(media+v[a])/10;
		}
		return media;
	}
	
}