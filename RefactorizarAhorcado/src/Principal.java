

import java.io.File;
import java.util.Scanner;

public class Principal {
	private final static byte NUM_PALABRAS = 20;
	private final static byte FALLOS = 7;
	private static String[] palabras = new String[NUM_PALABRAS];
	static String PSECRETA, RUTA = "src\\palabras.txt";
	static boolean ENCONTRADO;
	public static void main(String[] args) {
	/*Hemos llevado a metodos el programa para conseguir un main mas limpio
	y para ello hemos llevado la variable boolean encontrado a estatica para poder
	llevar a las diferentes funciones a metodos
	
	Hemos colocado variables estaticas y hemos realizado la refactorizacion
		
	*/	
		leerfichero();
		Scanner input = new Scanner(System.in);
		char[][] caracteresPalabra = seleccionarpalabra();
		String caracteresElegidos = "";
		int fallos;
		boolean acertado;
		System.out.println("Acierta la palabra");
		do {
			mostrarpalabras(caracteresPalabra);
			caracteresElegidos = introducirValor(input, caracteresElegidos);
			fallos = comprobarPalabra(caracteresPalabra, caracteresElegidos);
			menufallos(fallos);
			comprobarfallos(fallos);
			acertado = comprobarpalabra(caracteresPalabra);
		} while (!acertado && fallos < FALLOS);
		input.close();
	}

	/**
	 * @param caracteresPalabra
	 * @return
	 */
	private static boolean comprobarpalabra(char[][] caracteresPalabra) {
		boolean acertado;
		acertado = true;
		for (int i = 0; i < caracteresPalabra[1].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				acertado = false;
				break;
			}
		}
		if (acertado)
			System.out.println("Has Acertado ");
		return acertado;
	}

	private static void comprobarfallos(int fallos) {
		if (fallos >= FALLOS) {
			System.out.println("Has perdido: " + PSECRETA);
		}
	}

	private static void menufallos(int fallos) {
		switch (fallos) {
		case 1:

			System.out.println("     ___");
			break;
		case 2:

			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 3:
			System.out.println("  ____ ");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 4:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 5:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 6:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println("     ___");
			break;
		case 7:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println(" A   ___");
			break;
		}
	}

	private static int comprobarPalabra(char[][] caracteresPalabra, String caracteresElegidos) {
		int fallos;
		fallos = 0;
		
		for (int j = 0; j < caracteresElegidos.length(); j++) {
			ENCONTRADO = false;
			for (int i = 0; i < caracteresPalabra[0].length; i++) {
				if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
					caracteresPalabra[1][i] = '1';
					ENCONTRADO = true;
				}
			}
			if (!ENCONTRADO)
				fallos++;
		}
		return fallos;
	}

	private static String introducirValor(Scanner input, String caracteresElegidos) {
		System.out.println("Introduce una letra o acierta la palabra");
		System.out.println("Caracteres Elegidos: " + caracteresElegidos);
		caracteresElegidos += input.nextLine().toUpperCase();
		return caracteresElegidos;
	}

	private static char[][] seleccionarpalabra() {
		PSECRETA = palabras[(int) (Math.random() * NUM_PALABRAS)];

		char[][] caracteresPalabra = new char[2][];
		caracteresPalabra[0] = PSECRETA.toCharArray();
		caracteresPalabra[1] = new char[caracteresPalabra[0].length];
		return caracteresPalabra;
	}

	private static void leerfichero() {
		File fich = new File(RUTA);
		Scanner inputFichero = null;

		try {
			inputFichero = new Scanner(fich);
			for (int i = 0; i < NUM_PALABRAS; i++) {
				palabras[i] = inputFichero.nextLine();
			}
		} catch (Exception e) {
			System.out.println("Error al abrir fichero: " + e.getMessage());
		} finally {
			if (fich != null && inputFichero != null)
				inputFichero.close();
		}
	}

	private static void mostrarpalabras(char[][] caracteresPalabra) {
		System.out.println("####################################");

		for (int i = 0; i < caracteresPalabra[0].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				System.out.print(" -");
			} else {
				System.out.print(" " + caracteresPalabra[0][i]);
			}
		}
		System.out.println();
	}

}